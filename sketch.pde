float cardWidth;
float cardHeight;
float cardStackOffset;
int numColumns = 7;
int cardsInSuit = 13;
int numSuits = 4;
int numCards = cardsInSuit * numSuits;
int cardGridSize 
  = numColumns * cardsInSuit *2;
IntList[] table  = new IntList[numColumns];
IntList deck = new IntList();
boolean pMousePressed = false;
IntList cardsDragging = new IntList();
int draggingColumn = -1;

void setup()
{ 
  colorMode(HSB,255);
  cardWidth = width / float(numColumns);
  cardHeight = cardWidth * 1.5f;
  cardStackOffset = cardHeight / 3;
  
  for(int i = 0; i < 52; ++i)
  {
    deck.append(i+1);
  }
  deck.shuffle();
  
  for(int i = 0; i < numColumns; ++i)
  {
    table[i] = new IntList();
  }
  
  int curColumns = numColumns;
  int rtlColumn = 0;
  for(int i = 0; i < 28; ++i)
  {
    //int row = numColumns - curColumns;
    int column = (numColumns-1) - rtlColumn;
    
    //int index = (row * numColumns) + column;
    table[column].append(deck.remove(0));
    
    rtlColumn++;
    if(curColumns > 1 && rtlColumn >= curColumns)
    {
      rtlColumn = 0;
      --curColumns;
    }
  }
}

void draw()
{
  background(0);
  
  boolean pressPending 
    = mousePressed && !pMousePressed;
  
  if(deck.size() > 0)
  {
  int deckCard = deck.get(deck.size()-1);
  drawCard(deckCard, width - cardWidth, 0);
  
  if(pressPending && cardsDragging.size() ==0)
  {
    if(mouseOverCard(width-cardWidth,0))
    {
      draggingColumn=-1;
      cardsDragging.append(deck.remove(deck.size()-1));
      pressPending=false;
    }
  }
  }
  
  for(int i = 0; i < numColumns; ++i)
  {
    //print("COLUMN SIZE: " + table[i].size() + "\n");
    for(int k = 0; k < table[i].size(); ++k)
    {
      int row = k;
      int column = i;
      float cardPosX = cardWidth * column;
      float cardPosY = (cardStackOffset * row) + (cardHeight * 1.5);
      
      int card = table[i].get(k);
      
      drawCard(card,cardPosX,cardPosY);
      
      int j = (table[i].size()-1)-k;
      row = j;
      cardPosY = (cardStackOffset * row) + (cardHeight * 1.5);
      
      if(pressPending && cardsDragging.size() ==0)
      {
        if(mouseOverCard(cardPosX,cardPosY))
        {
          draggingColumn=i;
          while(table[i].size() > j)
          {
            cardsDragging.append(table[i].remove(j));
          }
          pressPending = false;
        }
      }
    }
  }
  
  if(cardsDragging.size() > 0)
  {
    for(int i = 0; i < cardsDragging.size(); ++i)
    {
    drawCard(cardsDragging.get(i),
      mouseX - (cardWidth/2.0),
      mouseY - (cardHeight/2.0) + (i*cardStackOffset));
    }
      
     if(!mousePressed)
     {
       int col = int(mouseX / cardWidth);
       int topCard = -1;
       if(table[col].size() > 0)
       {
         topCard = table[col].get(table[col].size()-1);
       }
       int cardDragging = cardsDragging.get(0);
       if((topCard == -1 && CardValue(cardDragging) == cardsInSuit)
       || (topCard != -1 
       && CardSuit(cardDragging) % 2 != CardSuit(topCard) % 2
       && CardValue(cardDragging) == CardValue(topCard) - 1))
       {
         print("CARD PLACED\n");
         while(cardsDragging.size() > 0)
         {
           table[col].append(cardsDragging.remove(0));
         }
       }
       else
       {
         print("CARD NOT PLACED\n");
         while(cardsDragging.size() > 0)
         {
           if(draggingColumn == -1)
           {
             deck.append(cardsDragging.remove(0));
           }
           else
           {
             table[draggingColumn].append(cardsDragging.remove(0));
           }
         }
       }
     }
  }
  
  pMousePressed = mousePressed;
}

boolean mouseOverCard(float cardPosX, float cardPosY)
{
  float mouseCardX = mouseX - cardPosX;
  float mouseCardY = mouseY - cardPosY;
  return mouseCardX > 0.0 &&
           mouseCardX < cardWidth &&
           mouseCardY > 0.0 &&
           mouseCardY < cardHeight;
}

int CardSuit(int card)
{
  return (card-1) / cardsInSuit;
}

int CardValue(int card)
{
  return ((card-1) % cardsInSuit)+1;
}

void drawCard(int card, float cardPosX, float cardPosY)
{
  int suit = CardSuit(card);
  int value = CardValue(card);
  fill(255);
      rect(cardPosX,cardPosY,cardWidth,cardHeight);
      fill(0, 255, suit % 2 == 0 ? 0 : 255);
      textSize(width / 20);
      String valString = str(value);
      switch(value)
      {
        case 1: valString = "A"; break;
        case 11: valString = "J"; break;
        case 12: valString = "Q"; break;
        case 13: valString = "K"; break;
      }
      switch(suit)
      {
        case 0: valString += " ♠️"; break;
        case 1: valString += " ❤️"; break;
        case 2: valString += " ♣️"; break;
        case 3: valString += " ♦️"; break;
      }
      text(valString,cardPosX,cardPosY + (width / 20));
}
